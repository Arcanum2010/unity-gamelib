﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CanvasGroup))]
public class GameUI : MonoBehaviour 
{
	public bool Shown
	{
		get{ return Panel.alpha > 0 && gameObject.activeSelf; }
	}

	public bool Enabled
	{
		get{ return _enabled; }
		set{ _enabled = value; }
	}

	public bool Transitioning
	{
		get{ return _transitioning != TransitionDirection.NotTransitioning; }
	}

    public CanvasGroup Panel
	{
		get
		{
			if(_panel == null)
                _panel = GetComponent<CanvasGroup>();
			return _panel;
		}
	}

    protected TransitionDirection _transitioning = TransitionDirection.NotTransitioning;
    protected enum TransitionDirection
    {
        Show,
        Hide,
        NotTransitioning
    }

	protected TransitionDirection QueuedTransition = TransitionDirection.NotTransitioning;

    List<UIShowHideListener> _showHideListeners = new List<UIShowHideListener>();

    CanvasGroup _panel;
	bool _enabled = true; // by default all GameUI's can be hidden or shown

	public void ListenForShowHideEvents(UIShowHideListener listener)
	{
		_showHideListeners.Add(listener);
	}
	public void StopListeningForShowHideEvents(UIShowHideListener listener)
	{
		_showHideListeners.Remove(listener);
	}

	/// <summary>
	/// If the UI is currently transitioning IN, will start hide immediately after
	/// </summary>
	public void QueueHide()
	{
		if(_transitioning == TransitionDirection.NotTransitioning || _transitioning == TransitionDirection.Show)
			QueuedTransition = TransitionDirection.Hide;
	}
	/// <summary>
	/// If the UI is currently transitioning OUT, will start show immediately after
	/// </summary>
	public void QueueShow()
	{
		if(_transitioning == TransitionDirection.NotTransitioning || _transitioning == TransitionDirection.Hide)
			QueuedTransition = TransitionDirection.Show;
	}

	public void LateShow() {
		StartCoroutine(LateShowCo());
	}

	IEnumerator LateShowCo() {
		yield return new WaitForSeconds(.3f);
		QueueShow();
	}

	public void Show()
	{
        if (_transitioning == TransitionDirection.Show) return;
		if(!Enabled) return;
		if(Shown) return;

		_transitioning = TransitionDirection.Show;
		_showHideListeners.ForEach(x => x.OnUIShown(this));


		if(!Application.isPlaying)
			_panel.alpha = 1;
		else
			StartCoroutine(OnShow());
	}

	public void Hide()
    {
        if (_transitioning == TransitionDirection.Hide) return;
		if(!Enabled) return;
        if (!Shown) return;

        _transitioning = TransitionDirection.Hide;
		_showHideListeners.ForEach(x => x.OnUIHidden(this));
		
		if(!Application.isPlaying)
			_panel.alpha = 0;
		else
			StartCoroutine(OnHide());
	}

	protected virtual IEnumerator OnShow()
	{
		Panel.alpha = 1;
        Panel.interactable = true;
        Panel.blocksRaycasts = true;
        //yield return new WaitForSeconds(0.3f);
        ClearTransitionState();
        yield return null;
	}

	protected virtual IEnumerator OnHide()
	{
        Panel.alpha = 0;
        Panel.interactable = false;
        Panel.blocksRaycasts = false;
		//yield return new WaitForSeconds(0.3f);
        ClearTransitionState();
        yield return null;
	}

	/*
	 * Helper Functions
	 */ 


    protected void ClearTransitionState()
    {
        _transitioning = TransitionDirection.NotTransitioning;
    }

	protected void LateUpdate()
	{
		if(QueuedTransition == TransitionDirection.Hide && _transitioning == TransitionDirection.NotTransitioning)
		{
			Hide();
			QueuedTransition = TransitionDirection.NotTransitioning;
		}
		else if(QueuedTransition == TransitionDirection.Show && _transitioning == TransitionDirection.NotTransitioning)
		{
			Show ();
			QueuedTransition = TransitionDirection.NotTransitioning;
		}

		OnLateUpdate();
	}


	protected virtual void OnLateUpdate()
	{

	}
}
