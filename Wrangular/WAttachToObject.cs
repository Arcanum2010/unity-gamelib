﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WAttachToObject : WModuleBase 
{
	
	public override bool IsExpressionMultipleExpressions {
		get {
			return false;
		}
	}

	public string Value;

	GameObject _obj;

	protected override string ExpressionString()
	{
		return "{" + Value + "}";
	}

	protected override void Apply(string propName, object val)
	{
		_obj = (GameObject)val;
	}

	protected virtual void LateUpdate()
	{
		if(_obj == null)
			return;
		transform.position = InputContextSystem.ConvertPoint(_obj.transform.position);
	}
}
