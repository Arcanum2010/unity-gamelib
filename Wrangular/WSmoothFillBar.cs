﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WSmoothFillFrame : WFillFrame
{
	public float FillSpeed = 1;

	// eventually move this out into a derived smooth fill bar...
	public ParticleSystem ParticlesToPlay;

	float _lastWidthPercent;
	float _currentWidthPercent;
	float _lastWidth;

	protected override void Apply(string propName, object val)
	{
		bool isFloat = val is float;

		// This will need an update every frame
		if (!isFloat)
		{
			Fill.rectTransform.sizeDelta = new Vector2(0, Fill.rectTransform.sizeDelta.y);
			return;
		}

		float f = (float)val;

		//Fill.width = (int)(f * Frame.width);
		_lastWidthPercent = f;
	}

	protected virtual void Update()
	{
		_currentWidthPercent = Mathf.Lerp(_currentWidthPercent, _lastWidthPercent, Time.deltaTime * FillSpeed);
        Fill.rectTransform.sizeDelta = new Vector2(_currentWidthPercent * Frame.rectTransform.sizeDelta.x, Fill.rectTransform.sizeDelta.y);

		// Only play the particles if we now have a greater fill than before
        if (_lastWidth < Fill.rectTransform.sizeDelta.x && ParticlesToPlay != null)
		{
			if(!ParticlesToPlay.isPlaying)
				ParticlesToPlay.Play();
		}

        _lastWidth = Fill.rectTransform.sizeDelta.x;
	}
}
