﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class WXboxMenuItem : WController 
{
    [HideInInspector]
    public int PlayerId;
    public bool IsSelected;
    public string OnSelect;

    WExpression _pressAExpression;
    Button _btn;

    protected override void OnControllerSetup()
    {
        _pressAExpression = Generate(OnSelect);
        _btn = GetComponent<Button>();
    }

    void Update()
    {
        if(InputSystem.Get.GetCurrentFilter() == InputSystem.FILTER_STANDARD && IsSelected)
        {
            if(_btn != null)
            {
                _btn.Select();
            }
            XboxControllerInput input = (XboxControllerInput)InputSystem.Get.GetPlayerInput(PlayerId);
            if(input.GetXboxButtonDown(XboxControllerInput.BTN_GREEN))
            {
                _pressAExpression.Evaluate();
            }
        }
        else
        {

        }
    }
}
