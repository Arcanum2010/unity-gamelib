﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class WConditionalColor : WModuleBase 
{
	
	public override bool IsExpressionMultipleExpressions {
		get {
			return false;
		}
	}

	public string Expression;
    public Color ColorIfTrue;
    public Color ColorIfFalse;
    Graphic _label;

	void Awake()
	{
        _label = GetComponent<Graphic>();
		if (_label == null)
			throw new System.Exception("[WConditionalColor] No label found on " + gameObject);
	}

	protected override void Apply(string propName, object val)
	{
		if(val is bool)
		{
            bool b = (bool)val;
            if (_label == null)
                _label = GetComponent<Graphic>();
                
            if (b)
                _label.color = ColorIfTrue;
            else
                _label.color = ColorIfFalse;
		}
	}

	protected override string ExpressionString()
	{
		return "{" + Expression + "}";
	}
}
