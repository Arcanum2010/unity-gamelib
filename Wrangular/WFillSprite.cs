﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class WFillSprite : WModuleBase 
{
	public override bool IsExpressionMultipleExpressions {
		get {
			return false;
		}
	}

	public string PercentExpression;

	Image _fillSprite;

	protected override void Start ()
	{
		base.Start ();
        _fillSprite = GetComponent<Image>();
	}

	protected override string ExpressionString ()
	{
		return "{" + PercentExpression + "}";
	}
	
	protected override WBindingSettings CreateBindSettings ()
	{
		WBindingSettings settings = base.CreateBindSettings ();
		settings.ForceApply = true;
		return settings;
	}

	protected override void Apply(string propName, object val)
	{
		bool isFloat = val is float;

		// This will need an update every frame
		if (!isFloat)
		{
			_fillSprite.fillAmount = 0;
			return;
		}
		float f = (float)val;

		_fillSprite.fillAmount = f;

	}

}
