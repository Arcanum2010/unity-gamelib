﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIShowHideListener : UIModuleBase 
{
    public override GameUI UI
    {
        get {
            if (_ui == null)
                _ui = transform.TraverseHierarchyFor<GameUI>();
            return _ui; 
        }
    }

	protected GameUI _GameUI
	{
		get{ return _ui; }
	}

    [SerializeField][HideInInspector]
	GameUI _ui;
	
	protected virtual void Awake()
	{
		_ui = transform.TraverseHierarchyFor<GameUI>();
		if(_ui != null)
			_ui.ListenForShowHideEvents(this);
	}

    public void Setup()
    {
        Awake();
    }

	public virtual void OnUIShown(GameUI ui)
	{
	}

	public virtual void OnUIHidden(GameUI ui)
	{

	}

}
