﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum InputListeningMode
{
    FirstPlayer,
    AnyPlayer,
    SpecificPlayer
}

public class WButtonListener : WModuleBase 
{
    public const float REPEAT_TIMER = 0.2f;
    public const float REPEAT_DELAY = 0.1f;

    public string IsActiveExpression;
    public string OnButtonPressExpression;
    public bool IsActive;
    public int ButtonId;
    public InputListeningMode ListeningMode;
    public int SpecificPlayerId;

    public bool CanRepeat;

    TimeRepeat _timeRepeater = new TimeRepeat(REPEAT_TIMER, REPEAT_DELAY);

    WExpression _onPress;

    public override bool IsExpressionMultipleExpressions
    {
        get { return false; }
    }

    protected override string ExpressionString()
    {
        return "{" + IsActiveExpression + "}";
    }

    protected override void Apply(string propName, object val)
    {
        IsActive = (bool)val;
    }

    protected override void Start()
    {
        base.Start();
        _onPress = Scope.Generate(OnButtonPressExpression);
    }

    void Update()
    {

        if(IsActive)
        {
            switch(ListeningMode)
            {
                case InputListeningMode.AnyPlayer:
                    for (int i = 0; i < InputSystem.Get.GetInputCount(); ++i)
                    {
                        XboxControllerInput xbox_input = (XboxControllerInput)InputSystem.Get.GetPlayerInput(i + 1);

                        bool repeat_down = CanRepeat && _timeRepeater.Update(Time.deltaTime, xbox_input.GetXboxButton(ButtonId));

                        if ((!CanRepeat && xbox_input.GetXboxButtonDown(ButtonId)) || repeat_down)
                        {
                            _onPress.Evaluate();
                            break;
                        }
                    }
                    break;
                case InputListeningMode.FirstPlayer: 
                    XboxControllerInput xbox_input_first_player = (XboxControllerInput)InputSystem.Get.GetPlayerInput(1);

                    bool repeat_down_first = CanRepeat && _timeRepeater.Update(Time.deltaTime, xbox_input_first_player.GetXboxButton(ButtonId));

                    if (repeat_down_first || (!CanRepeat && xbox_input_first_player.GetXboxButtonDown(ButtonId)))
                    {
                        _onPress.Evaluate();
                    }
                    break;
                case InputListeningMode.SpecificPlayer: 
                    XboxControllerInput xbox_input_specific = (XboxControllerInput)InputSystem.Get.GetPlayerInput(SpecificPlayerId);

                    bool repeat_down_specific = CanRepeat && _timeRepeater.Update(Time.deltaTime, xbox_input_specific.GetXboxButton(ButtonId));

                    if (repeat_down_specific || (!CanRepeat && xbox_input_specific.GetXboxButtonDown(ButtonId)))
                    {
                        _onPress.Evaluate();
                        break;
                    }
                    break;
            }
        }
    }
}
