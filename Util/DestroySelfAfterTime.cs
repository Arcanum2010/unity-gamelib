﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class DestroySelfAfterTime : MonoBehaviour 
{
    public float TimeUntilSelfDestruct;

    FrameTimer _timer;

	void Awake()
	{
        _timer = new FrameTimer(TimeUntilSelfDestruct);
	}

    void Update()
    {
        if(!_timer.NextFrame(Time.deltaTime))
        {
            Destroy(gameObject);
        }
    }

}
