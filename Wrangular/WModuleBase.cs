﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;

public class ModuleExpression : System.Attribute
{

}

public class PopulateFromConstants : System.Attribute
{
    public string ConstantPrefix;
    public System.Type ClassType;

    public PopulateFromConstants(System.Type constantClassType, string constant_prefix)
    {
        ConstantPrefix = constant_prefix;
        ClassType = constantClassType;
    }

    public Dictionary<string,object> GetConstantValues()
    {
        FieldInfo[] constant_fields = ClassType.GetConstants();
        Dictionary<string, object> values = new Dictionary<string, object>();
        foreach(FieldInfo info in constant_fields)
        {
            /*
            if(info.Name.IndexOf(ConstantPrefix) == 0)
            {
                string name = info.Name;
                if(.Replace(ConstantPrefix,"");
                */
                values.Add(info.Name, info.GetRawConstantValue());
            //}
        }

        return values;
    }
}

public abstract class WModuleBase : MonoBehaviour {

	
    [Tooltip("This binding will be executed every time Wrangular runs an Apply(), as opposed to waiting behind other bindings.")]
	public bool ForceApply = false;
    [Tooltip("Will not include this object in the search for a Wrangular Scope.")]
    public bool ExcludeSelf = false;

	/// <summary>
	/// Is the expression associated with this module actually multiple expressions, 
	/// denoted by enclosing '{' '}' pairs?
	/// </summary>
	public abstract bool IsExpressionMultipleExpressions
	{
		get;
	}

	public WScope Scope
	{
		get{ return _scope; }
	}

	public bool WillApply
	{
		get{
			if (_bindSettings == null) return false;
			return _bindSettings.ShouldEvaluate; 
		}

	}

	protected WBindingSettings BindSettings
	{
		get{ return _bindSettings; }
	}


	[SerializeField][HideInInspector]
	WScope _scope;
	WBindingSettings _bindSettings;

	public void AcquireScope()
	{
		_scope = transform.TraverseHierarchyFor<WScope>(ExcludeSelf);
		if(_scope == null)
			throw new System.Exception(this.gameObject + ": No scope found");
	}

	public string GetExpression()
	{
		return ExpressionString();
	}

    public void ManualSetup()
    {
        Start();
    }

	protected virtual void Start()
	{
		BindExpression(ExpressionString());
	}
	protected abstract string ExpressionString();

	protected virtual WBindingSettings CreateBindSettings()
	{
		_bindSettings =  new WBindingSettings()
		{
			Apply = Apply,
			ForceApply = ForceApply // So that hides aren't all staggered n stuff
		};
		return _bindSettings;
	}
	protected void BindExpression(string e)
	{
		AcquireScope();
		CreateBindSettings();

		_scope.Bind(this,e, _bindSettings);
	}

	protected void BindRawExpression(string e)
	{
		AcquireScope();
		CreateBindSettings();

		_scope.Bind(this, "{" + e + "}", _bindSettings);
	}

	protected abstract void Apply(string propName, object val);
}
