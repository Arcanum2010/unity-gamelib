﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class WFillFrame : WModuleBase 
{
	
	public override bool IsExpressionMultipleExpressions {
		get {
			return false;
		}
	}

	public Image Fill;
    public Image Frame;

	public string PercentExpression;

	protected override void Apply(string propName, object val)
	{
		bool isFloat = val is float;

		// This will need an update every frame
		if (!isFloat)
		{
			Fill.rectTransform.sizeDelta = new Vector2(0, Fill.rectTransform.sizeDelta.y);
			return;
		}

		float f = (float)val;

        Fill.rectTransform.sizeDelta = new Vector2((int)(f * Frame.rectTransform.sizeDelta.x), Fill.rectTransform.sizeDelta.y);

	}

	protected override WBindingSettings CreateBindSettings ()
	{
		WBindingSettings settings = base.CreateBindSettings ();
		settings.ForceApply = true;
		return settings;
	}

	protected override string ExpressionString ()
	{
		return "{" + PercentExpression + "}";
	}
}
