﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Text;
using Wrangular.Compiler;

/// <summary>
/// WExpressions are basically strings converted into reflection calls. 
/// For example.. Item.MyProperty.MyFunction() would be a valid expression
/// that is converted into a series of FieldInfo/PropertyInfo.GetValue's and MethodInfo.Invoke's
/// 
/// You can't directly turn a string into a WExpression though. You must first run it through a 
/// lexer so that it is tokenized (hence the constructor)
/// </summary>
public class WExpression
{
	// Turning this on will add all expressions to a 'total expressions list' for debug output.
	public const bool DEBUG_EXPRS = false;

    /// <summary>
    /// Represents an empty expression that will evaluate to NULL
    /// </summary>
    public WExpression()
    {
        _expression = new LiteralExpression(null);
    }

    /// <summary>
    /// Expressions require a token stream, and a scope they are associated with.
    /// 
    /// If you want to generate an expression, find a scope and call the Generate(string expr) method on it.
    /// (That method will lex it for you and associate the expression with the right scope)
    /// </summary>
    public WExpression(WScope scope, List<WToken> tokens)
    {
        _scope = scope;
        _expressionStack = new List<InternalExpression>();
        _tokens = tokens;
        if (tokens.Count == 0)
        {
            _expression = new LiteralExpression(null);
            return;
        }

        //generateExpression();

        generateExpression_shiftReduce();
    }

    /// <summary>
    /// Evaluates the expression to a value (cast it to whatever you're expecting)
    /// </summary>
    public object Evaluate()
    {
        _lastEvaluation = _expression.Evaluate();
        return _lastEvaluation;
    }

    /// <summary>
    /// Attempts to evaluate the type this expression will return. This can be as expensive of a call as Evaluate()
    /// </summary>
    public System.Type EvaluateSemantics(Wrangular.Compiler.SymbolTable table = null)
    {
        return _expression.EvaluateSemantics(table);
    }

    /// <summary>
    /// Problem? Dump a bunch of information to try to help us solve it.
    /// </summary>
    public void Dump()
    {

        Debug.Log("Expression Dump");
        Debug.Log("Source Scope --- " + _scope);
        if(_consumedTokens != null)
            Debug.Log("Consumed Tokens --- " + dumpTokens(_consumedTokens));
        Debug.Log("Returned value --- " + ((_lastEvaluation != null) ? _lastEvaluation : "NULL"));
        string estack = "";
        foreach (InternalExpression e in _totalExpressionStack)
        {
            estack += e.GetType() + " - " + e.Dump() + "\n";
        }
        Debug.Log(estack);
        estack = "";
        if (_expressionStack.Count > 0)
        {
            Debug.Log("Remaining in stack:");
            foreach (InternalExpression e in _expressionStack)
            {
                estack += e.GetType() + " - " + e.Dump() + "\n";
            }
            Debug.Log(estack);
        }
        if (_expression == null)
        {
            Debug.Log("Expr was null.");
            return;
        }

        Debug.Log(_expression.Dump());
    }

    /*
     * If you have to read further than this, God Bless You 
     * 
     * 
     * and God Bless America
     */

    /// <summary>
    /// Used to compile expressions to CSharp. Read further on InternalExpression.GenerateCSharp...
    /// </summary>
    public CSharpExpression GenerateCSharp(Wrangular.Compiler.SymbolTable table)
    {
        return _expression.GenerateCSharp(table);
    }

    /// <summary>
    /// WExpressions are composed of smaller expressions that are chained together
    /// </summary>
	private abstract class InternalExpression
	{

        /// <summary>
        /// Evaluates all subtrees of expressions (Up to the subclasses how this is done) and
        /// returns a value.
        /// </summary>
		public abstract object Evaluate();

        /// <summary>
        /// Goes through the steps of evaluation, except the goal is to determine the resulting type of
        /// the expression WITHOUT getting actual values returned (doesn't call functions or properties.)
        /// </summary>
		public abstract System.Type EvaluateSemantics(Wrangular.Compiler.SymbolTable table = null);

        /// <summary>
        /// Used to compile WExpressions into yet another kind of expression that can be used to write
        /// the expression as CSharp code.
        /// </summary>
        public abstract CSharpExpression GenerateCSharp(Wrangular.Compiler.SymbolTable table);

        /// <summary>
        /// Dumps out some information about this expression
        /// </summary>
		public abstract string Dump();
	}

    // This is basically a + b.. a && b.. any other binary operation
	private class BinaryOpExpression : InternalExpression
	{
        public InternalExpression A { get { return _a; } }
        public InternalExpression B { get { return _b; } }
        public string Operator { get { return _op; } }

		string _op;
		InternalExpression _a; // Op 1
		InternalExpression _b; // Op 2

		public BinaryOpExpression(string op)
		{
			_op = op;
		}

		public override CSharpExpression GenerateCSharp(Wrangular.Compiler.SymbolTable table)
		{
			return new CSharpBinOpExpression(_a.GenerateCSharp(table), _b.GenerateCSharp(table), _op);
		}

		public void SetA(InternalExpression a)
		{
			if (a == null) throw new System.ArgumentNullException("a");
			_a = a;
		}
		public void SetB(InternalExpression b)
		{
			if (b == null) throw new System.ArgumentNullException("b");
			_b = b;
		}

		public override System.Type EvaluateSemantics(Wrangular.Compiler.SymbolTable table = null)
		{
			System.Type aType = _a.EvaluateSemantics(table);
			System.Type bType = _b.EvaluateSemantics(table);

			if(aType != bType)
			{
				// Add part here eventually to check for overloaded binop method
				throw new System.Exception("Cannot add " + aType + " and " + bType + ". Must be of same type.");
			}

			return aType;
		}


		public override object Evaluate()
		{
			if (_a == null)
			{
				throw new System.ArgumentException("Expression A was not set.");
			}
			else if (_b == null)
			{
				throw new System.ArgumentException("Expression B was not set.");

			}

			object a = _a.Evaluate();
			object b = _b.Evaluate();

			if (a == null)
				return 0;
			if (b == null)
				return 0;

			System.Type t = a.GetType();
			switch (_op)
			{
				case "+":
					if (a is int && b is int) return (int)a + (int)b;
					if (a is string && b is string) return (string)a + (string)b;

                    // This is the reflection way to call an overloaded addition operator
					return t.GetMethod("op_Addition").Invoke(null, new object[] { a, b });
				case "-":
					if (a is int && b is int) return (int)a - (int)b;
					return t.GetMethod("op_Subtraction").Invoke(null, new object[] { a, b });
				case "/":
					if (a is int && b is int) return (int)a / (int)b;
					return t.GetMethod("op_Division").Invoke(null, new object[] { a, b });
				case "*":
					if(a is float && b is float) return (float)a * (float)b;
					if(a is int && b is float) return (int)a * (float)b;
					if(a is float && b is int) return (float)a * (int)b;
					if (a is int && b is int) return (int)a * (int)b;
					return t.GetMethod("op_Multiply").Invoke(null, new object[] { a, b });
				case "%":
					if (a is int && b is int) return (int)a % (int)b;
					return t.GetMethod("op_Modulus").Invoke(null, new object[] { a, b });
				case ">":
					if (a is int && b is int) return (int)a > (int)b;
                    // Add support for floats here (and below in simlar manner)
                    throw new System.Exception("IDK " + a.GetType() + " " + b.GetType());
				case "<":
					if (a is int && b is int) return (int)a < (int)b;
                    throw new System.Exception("IDK " + a.GetType () + " " + b.GetType());
				case "<=":
					if (a is int && b is int) return (int)a <= (int)b;
                    throw new System.Exception("IDK " + a.GetType () + " " + b.GetType());
				case ">=":
					if (a is int && b is int) return (int)a >= (int)b;
                    throw new System.Exception("IDK " + a.GetType () + " " + b.GetType());
                case "&&":
                    if (a is bool && b is bool) return (bool)a && (bool)b;
                    if (a is int && b is int) return (int)a > 0 && (int)b > 0;
                    if (a is bool && b is int) return (bool)a && (int)b > 0;
                    if (a is int && b is bool) return (int)a > 0 && (bool)b;
                    throw new System.Exception("IDK " + a.GetType () + " " + b.GetType());
                case "||":
                    if (a is bool && b is bool) return (bool)a || (bool)b;
                    if (a is int && b is int) return (int)a > 0 || (int)b > 0;
                    if (a is bool && b is int) return (bool)a || (int)b > 0;
                    if (a is int && b is bool) return (int)a > 0 || (bool)b;
                    throw new System.Exception("IDK " + a.GetType () + " " + b.GetType());
                case "==":
                    return a.Equals(b);
                case "!=":
                    return !a.Equals(b);
				default:
					throw new System.Exception("Unknown Binary Operation: " + _op);
			}
		}

		public override string Dump()
		{
			string a_dump = _a == null ? "NULL" : _a.Dump();
			string b_dump = _b == null ? "NULL" : _b.Dump();

			return a_dump + " " + _op + " " + b_dump;
		}

	}
	
    // This represents a list of expressions that all need to be evaluated separately, 
    // but bound together for things like function calls.
    private class ExpressionList : InternalExpression
	{
		public int ExpressionCount
		{
			get { return _expressions.Count; }
		}

        public List<InternalExpression> Expression { get { return _expressions; } }

		List<InternalExpression> _expressions;

		public ExpressionList()
		{
			_expressions = new List<InternalExpression>();
		}

		public override CSharpExpression GenerateCSharp(Wrangular.Compiler.SymbolTable table)
		{
			List<CSharpExpression> exprs = new List<CSharpExpression>();
			foreach (InternalExpression e in _expressions)
				exprs.Add(e.GenerateCSharp(table));

			CSharpArgListExpression argList = new CSharpArgListExpression(exprs);
			return argList;
		}

		public void AddExpression(InternalExpression e)
		{
			_expressions.Insert(0, e);
		}

		public override object Evaluate()
		{
			object[] parameters = new object[_expressions.Count];

			for(int i = 0; i < _expressions.Count; ++i)
			{
				parameters[i] = _expressions[i].Evaluate();
			}

			return parameters;
			//return _expressions.Select(x => x.Evaluate()).ToArray();
		}

		public override string Dump()
		{
			string dump = "(";
			int i = 0;
			foreach (InternalExpression e in _expressions)
			{
				dump += e.Dump();
				if (i < _expressions.Count - 1)
					dump += ",";
			}
			dump += ")";
			return dump;
		}

		public override System.Type EvaluateSemantics(Wrangular.Compiler.SymbolTable table = null)
		{
			List<System.Type> types = _expressions.Select(x => x.EvaluateSemantics(table)).ToList();
			if (types.Count > 0)
				return System.Linq.Expressions.Expression.GetFuncType(types.ToArray());
			else 
				return typeof(System.Action);
		}
	}
	
    // These are pretty simple and just contain a value that is returned when Evaluate()'d
    private class LiteralExpression : InternalExpression
	{
        public object Value { get { return _value; } }
		object _value;

		public LiteralExpression(object value)
		{
			_value = value;
		}

		public override CSharpExpression GenerateCSharp(Wrangular.Compiler.SymbolTable table)
		{
			if (_value == null)
			{
				return new CSharpLiteralExpression();
			}

			return new CSharpLiteralExpression(_value.ToString(), _value.GetType());
		}

		public override object Evaluate()
		{
			return _value;
		}

		public override string Dump()
		{
			return "L[" + _value + "]";
		}

		public override System.Type EvaluateSemantics(Wrangular.Compiler.SymbolTable table = null)
		{
			return _value.GetType();
		}
	}
	
    // Function calls!
    private class FuncExpression : InternalExpression
	{
        public IDExpression FuncName { get { return _funcName; } }
        public ExpressionList Parameters { get { return _parameters; } }

		IDExpression _funcName;
		ExpressionList _parameters;
		MethodInfo _method;


		public FuncExpression(IDExpression funcName, ExpressionList parameters)
		{
			_funcName = funcName;
			_parameters = parameters;
		}

		public override CSharpExpression GenerateCSharp(Wrangular.Compiler.SymbolTable table)
		{
			//return _funcName.GenerateCSharp(table) + _parameters.GenerateCSharp(table);
			CSharpExpression funcIdExpr = _funcName.GenerateCSharp(table);
			funcIdExpr.ShouldSkipNullCheck = true;

			_method = GetMethodInfo(table);

			if (_parameters.ExpressionCount < _method.GetParameters().Length)
				throw new System.Exception("Got wrong amount of args for " + _funcName.Id);

			CSharpFuncExpression func = new CSharpFuncExpression(
				_funcName.Id,
				EvaluateSemantics(table),
				_parameters.ExpressionCount,
				funcIdExpr,
				(CSharpArgListExpression)_parameters.GenerateCSharp(table)
				);
			return func;
		}

		public override object Evaluate()
		{
			object[] parameters = (object[])_parameters.Evaluate();

            // And this is where I would call my delegates.. IF I HAD ANY
			
            //if (_delegateMethod != null)
			//{
			//	return _delegateMethod.DynamicInvoke(parameters);
			//}

            object obj = null;
            System.Type type = _funcName.GetContextType(out obj);
            

			if (obj == null)
				return null;

			if(_method == null)
				_method = type.GetMethod(_funcName.Id);

			// This doesn't quite work? I forget what the problem is.
            // The idea is that delegates are faster to call than method invoke() is.
			/*
			if (_delegateMethod == null)
			{
				ParameterInfo[] parameterInfos = _method.GetParameters();
				if (parameterInfos.Length > 0)
				{
					List<System.Type> types = new List<System.Type>(_method.GetParameters().Select(x => x.ParameterType));
					types.Add(_method.ReturnType);

					_delegateMethod = System.Delegate.CreateDelegate(System.Linq.Expressions.Expression.GetFuncType(types.ToArray()), obj, _method.Name);
				}
				else
				{
					_delegateMethod = System.Delegate.CreateDelegate(System.Linq.Expressions.Expression.GetFuncType(_method.ReturnType), obj, _method.Name);
				}

				return _delegateMethod.DynamicInvoke(parameters);
			}
			*/

			if (_method == null)
				throw new System.Exception("Could not find method with name '" + _funcName.Id + "' on " + obj);

			// Legacy invocation

            if (!_method.DeclaringType.IsAssignableFrom(obj.GetType()))
                throw new System.Exception("object was of type " + obj.GetType() + " - expected " + _method.DeclaringType + " " + type);

			return _method.Invoke(obj, parameters);
		}

		public override string Dump()
		{
			return _funcName.Dump() + _parameters.Dump();
		}

		public MethodInfo GetMethodInfo(Wrangular.Compiler.SymbolTable table)
		{
			System.Type typeWithMethod = _funcName.GetDeclaringTypeForMethod(_funcName.Id, table);

			if (typeWithMethod == null)
				throw new System.Exception("aaa");

			MethodInfo info = typeWithMethod.GetMethod(_funcName.Id);

			return info;
		}

		public override System.Type EvaluateSemantics(Wrangular.Compiler.SymbolTable table = null)
		{
			System.Type parameterTypes = _parameters.EvaluateSemantics(table);
			System.Type typeWithMethod = _funcName.GetDeclaringTypeForMethod(_funcName.Id, table);
			System.Type[] genericArgs = parameterTypes.GetGenericArguments();

			if (typeWithMethod == null)
				throw new System.Exception("aaa");

			MethodInfo info = typeWithMethod.GetMethod(_funcName.Id);
			if (info == null)
			{
				throw new System.Exception("Could not find method '" + _funcName.Id + "' on type " + typeWithMethod);
			}

			int parameterId = 0;
			foreach(ParameterInfo parameter in info.GetParameters())
			{
				if(parameter.ParameterType != genericArgs[parameterId])
				{
					throw new System.Exception("Argument types do not match method signature for '" + 
					                           _funcName.Id + "' on " + 
					                           _funcName.GetContext() + " (expected='" + genericArgs[parameterId] + "' got='" + parameter.ParameterType + "')");
				}
				parameterId++;
			}

			return info.ReturnType;
		}
	}
	
    // Indexing into an array
    private class ArrayIndexExpression : InternalExpression
	{
		IDExpression _id;
		InternalExpression _index;

		public ArrayIndexExpression(IDExpression id, InternalExpression index)
		{
			_id = id;
			_index = index;
		}

		public override CSharpExpression GenerateCSharp(Wrangular.Compiler.SymbolTable table)
		{

			return new CSharpUnhandledExpression("ArrayIndex", "Didn't do this yet!");
			//return _id.GenerateCSharp(table) + "[" + _index.GenerateCSharp(table) + "]";
		}

		public override object Evaluate ()
		{
			object arrayObj = _id.Evaluate();
			IEnumerable enumerable = (IEnumerable)arrayObj;

			if(enumerable == null)
			{
				return null;
			}
			
			if(!(arrayObj is IEnumerable))
				throw new System.Exception("Trying to index into a non enumerable type. Got: " + arrayObj.GetType());

			int index = (int)_index.Evaluate();

			IEnumerator enumerator = enumerable.GetEnumerator();

			int i = 0;

			object val = null;
			while(enumerator.MoveNext() && i < index)
			{
				val = enumerator.Current;
				i++;
			}

			return val;
		}
		
		public override string Dump ()
		{
			return "Index: " + _index.Dump() + ", expr: " + _id.Dump();
		}

		public override System.Type EvaluateSemantics(Wrangular.Compiler.SymbolTable table = null)
		{
			System.Type idType = _id.EvaluateSemantics(table);

			if(idType.IsSubclassOf(typeof(IEnumerable)) || idType == typeof(IEnumerable))
				return idType;

			throw new System.Exception("Trying to index into a non enumerable variable '" + _id.Id + "'");
		}
	}
	
    // Any sort of identifier.. Could be used for functions, properties, fields ... whatever!
    private class IDExpression : InternalExpression
	{

		public string Id
		{
			get { return _id; }
		}

        public InternalExpression ParentExpression { get { return _e; } }
        public WScope ParentScope { get { return _scope; } }

        public bool IsScope { get { return _isScope; } }

		InternalExpression _e;
		WScope _scope;

		//FieldInfo _field;
		//PropertyInfo _property;
		ScopeFieldInfo _field;
		ScopePropertyInfo _property;

		string _id;
		int _index;

		bool _isScope;
		bool _hasIndex;
		

		public IDExpression(string id, WScope scope)
		{
			_scope = scope;
			_id = id;
			_isScope = true;
		}

		public IDExpression(string id, InternalExpression e)
		{
			_e = e;

			_id = id;

		}

		public IDExpression WithIndex(int index)
		{
			_hasIndex = true;
			_index = index;
			return this;
		}

		public override CSharpExpression GenerateCSharp(Wrangular.Compiler.SymbolTable table)
		{
			System.Type varType = EvaluateSemantics(table);
			bool isInferred = table.HasInferredType(_id);

			if (!_isScope)
			{
				CSharpExpression existingId = _e.GenerateCSharp(table);
				CSharpExpression idVarExpr = null;
				idVarExpr = new CSharpVarExpression(_id, varType, existingId);
				idVarExpr.IsInferred = isInferred;
				return idVarExpr;
			}

			WScope context = (WScope)GetContext();
			Wrangular.Compiler.Symbol s = table.GetSymbol(context);

			CSharpVarExpression scopeVar = new CSharpVarExpression(s.ToString(), System.Type.GetType(s.Type));

			CSharpVarExpression varExpr = new CSharpVarExpression(_id, varType, scopeVar);
			
			varExpr.IsInferred = isInferred;
			

			return varExpr;
		}

        public System.Type GetContextType(out object obj)
        {
            if (_isScope)
            {
                if (_scope.GetField(_id) != null)
                {
                    obj = _scope.GetField(_id).Scope;
                    return _scope.GetField(_id).Scope.GetType();
                }
                if (_scope.GetProperty(_id) != null)
                {
                    obj = _scope.GetProperty(_id).Scope;
                    return _scope.GetProperty(_id).Scope.GetType();
                }
                if (_scope.GetMethod(_id) != null)
                {
                    obj = _scope.GetMethod(_id).Scope;
                    return _scope.GetMethod(_id).Scope.GetType();
                }

                obj = _scope;
                return _scope.GetType();
                //throw new System.Exception("Could not find ID '" + _id + "' on any scope- starting at " + _scope);
            }

            object o = _e.Evaluate();
            obj = o;
            if(_e is IDExpression)
            {
                IDExpression id_expr = (IDExpression)_e;

                if (id_expr._field != null) return id_expr._field.Field.FieldType;
                if (id_expr._property != null) return id_expr._property.Property.PropertyType;
            }
            return o.GetType();
        }

		public object GetContext()
		{
			if (_isScope)
			{
				if (_scope.GetField(_id) != null)
					return _scope.GetField(_id).Scope;
				if (_scope.GetProperty(_id) != null)
					return _scope.GetProperty(_id).Scope;
				if (_scope.GetMethod(_id) != null)
				{
					return _scope.GetMethod(_id).Scope;
				}

				return _scope;
				//throw new System.Exception("Could not find ID '" + _id + "' on any scope- starting at " + _scope);
			}
			return _e.Evaluate();
		}

		public override object Evaluate()
		{
			if(_isScope)
			{
				if (_scope.HasFixedProp(_id)) return _scope.GetFixedProp(_id);
			}

			if (_field != null)
			{
				if (_isScope)
				{
					return _field.GetValue(_field.Scope);
				}
				else
				{
					object o = _e.Evaluate();
					if (o == null) return null;

					return _field.GetValue(o);
				}

			}
			if(_property != null)
			{
				if(_isScope)
				{
					if (_hasIndex)
					{
						return _property.GetValue(_property.Scope, new object[] { _index });
					}
					else
					{
						return _property.GetValue(_property.Scope, null);
					}
				}else{
					object o = _e.Evaluate();
					if (o == null) return null;

					if (_hasIndex)
					{
						return _property.GetValue(o, new object[] { _index });
					}
					else
					{
						return _property.GetValue(o, null);
					}
				}
			}

			ScopeFieldInfo f = null;
			ScopePropertyInfo p = null;
			if (_isScope)
			{

				f = _scope.GetField(_id);
				if (f != null)
				{
					_field = f;
					return f.GetValue(f.Scope);
				}

				p = _scope.GetProperty(_id);
				if (p != null)
				{
					_property = p;
					if (_hasIndex)
						return p.GetValue(p.Scope, new object[] { _index });
					return p.GetValue(p.Scope, null);
				}

				throw new System.Exception("ID not found '" + _id + "' on " + GetContext());
			}
			else
			{
				object o = _e.Evaluate();

				if (o == null) // don't fail!
					return null; 

				System.Type type = o.GetType();
				f = new ScopeFieldInfo() { Field = type.GetField(_id) };

				// Try to acquire a field
				if (f.Field != null)
				{
					_field = f;
					return f.GetValue(o);
				}

				// Try to acquire a property
				p = new ScopePropertyInfo() { Property = type.GetProperty(_id) };
				if (p.Property != null)
				{
					_property = p;
					if (_hasIndex)
						return p.GetValue(o, new object[] { _index });
					return p.GetValue(o, null);
				}


				throw new System.Exception("ID not found '" + _id + "' on " + GetContext());
			}
		}

		public override string Dump()
		{
			if (_isScope)
				return _id + "::" + GetContext();
			else
				return _id + "::" + _e.Dump();
		}

		public System.Type GetDeclaringTypeForMethod(string methodName, Wrangular.Compiler.SymbolTable table)
		{
			if (_isScope)
			{
				ScopeMethodInfo m = _scope.GetMethod(methodName);
				if (m != null)
					return m.Scope.GetType();
			}
			else
			{
				return _e.EvaluateSemantics(table);
			}
			return null;
		}

		/// <summary>
		/// If this returns null, this means the type needs to be inferred
		/// </summary>
		public override System.Type EvaluateSemantics (Wrangular.Compiler.SymbolTable table = null)
		{
			if(_isScope)
			{
				if (_scope.GetFixedProp(_id) != null) return _scope.GetFixedProp(_id).GetType();
				if (_scope.GetProperty(_id) != null) return _scope.GetProperty(_id).Property.PropertyType;
				if (_scope.GetField(_id) != null) return _scope.GetField(_id).Field.FieldType;
				if (_scope.GetMethod(_id) != null) return _scope.GetMethod(_id).Method.ReturnType;
				if (_scope.HasFixedProp(_id))
				{
					if(table != null)
						return table.GetInferredType(_id);
				}

				throw new System.Exception("Could not find id '" + _id + "' on scope " + _scope);
			}else{
				System.Type parentExprType = _e.EvaluateSemantics(table);

				if (parentExprType.GetField(_id) != null) return parentExprType.GetField(_id).FieldType;
				if (parentExprType.GetProperty(_id) != null) return parentExprType.GetProperty(_id).PropertyType;
				if (parentExprType.GetMethod(_id) != null) return parentExprType.GetMethod(_id).ReturnType;

				string s = Dump();
				Debug.Log(s);
				Debug.Log(table.DumpInfo());
				throw new System.Exception("Could not find id '" + _id + "' in " + parentExprType.Name + " in scope " + _scope);
			}
		}
	}

	InternalExpression _expression;
	List<WToken> _tokens;
	List<WToken> _consumedTokens;
    List<InternalExpression> _expressionStack;
    WScope _scope;

    // Used for DEBUG
    List<InternalExpression> _totalExpressionStack = new List<InternalExpression>();

    // Returns the next token on the token stack
	WToken next()
	{
		if (_tokens.Count == 0)
			return null;
		return _tokens[0];
	}

    // Shifts the top internal token off of the token stack
	WToken shift()
	{
		if (_tokens.Count == 0)
			return null;
		WToken t = _tokens[0];
		_tokens.RemoveAt(0);
		_consumedTokens.Add(t);
		return t;
	}


    // Pop an internal expression off the internal stack (for internal use)
	InternalExpression pop()
	{
		if (_expressionStack.Count == 0)
			return null;
		InternalExpression e = _expressionStack[_expressionStack.Count - 1];
		_expressionStack.RemoveAt(_expressionStack.Count - 1);
		return e;
	}

    // Push an internal expression onto the internal stack (in an internal manner)
	void push(InternalExpression e)
	{
		if (DEBUG_EXPRS)
			_totalExpressionStack.Add(e);
		_expressionStack.Add(e);
	}

    // Internally gets the internal expression at the back of the internal stack.
	InternalExpression back()
	{
		if (_expressionStack.Count == 0)
			return null;
		InternalExpression e = _expressionStack[_expressionStack.Count - 1];
		return e;
	}

    /*
     * someFun(a + b) || someMoreFun(c * d)
     * 
     *    parse stack               look ahead              action
     * 1. ID                        '('                     shift
     * 2. ID'('                     ID                      shift
     * 3. ID'('ID                   '+'                     shift
     * 4. ID'('ID'+'ID'
     * 5. ID'('BIN_OP_EXPR
     * 6. ID'('BIN_OP_EXPR')'
     * 7. FUNC_EXPR
     * 8. FUNC_EXPR'||'
     * 9. FUNC_EXPR'||'ID           '('
     * 10. FUNC_EXPR'||'ID'('
     * 11. FUNC_EXPR'||'ID'('ID
     * 12. FUNC_EXPR'||'ID'('ID'*'
     * 13. FUNC_EXPR'||'ID'('ID'*'ID
     * 14. FUNC_EXPR'||'ID'('ID'*'ID
     * 15. FUNC_EXPR'||'ID'('BIN_OP_EXPR
     * 16. FUNC_EXPR'||'ID'('BIN_OP_EXPR')'
     * 17. FUNC_EXPR'||'FUNC_EXPR
     * 18. BIN_OP_EXPR
     * 
     */

    class ParseStackEntry
    {
        public bool IsToken { get { return _token != null; } }
        public bool IsExpression { get { return _expression != null; } }

        public WToken Token { get { return _token; } }
        public InternalExpression Expression { get { return _expression; } }

        WToken _token;
        InternalExpression _expression;

        public ParseStackEntry(WToken token)
        {
            _token = token;
        }

        public ParseStackEntry(InternalExpression expression)
        {
            _expression = expression;
        }

        public override string ToString()
        {
            if (IsToken) return "(Token) " + Token.ToString();
            if (IsExpression) return Expression.GetType() + " (Expr) " + Expression.Dump();
            return "!! Invalid Parse Stack Entry !!";
        }
    }

    // EXPR '+' EXPR -> BIN_OP_EXPR
    static ParseStackEntry REDUCE_BIN_OP_EXPR(List<ParseStackEntry> entries, WScope scope)
    {
        if(entries.Count < 3)
            return null;

        ParseStackEntry op1 = entries[entries.Count - 3];
        ParseStackEntry op2 = entries[entries.Count - 1];
        ParseStackEntry op = entries[entries.Count - 2];

        if (!op1.IsExpression || !op2.IsExpression)
            return null;
        if (!op.IsToken || op.Token.Type != WToken.OPERATOR)
            return null;
        if (op.Token.Value == "[" || op.Token.Value == "]")
            return null;
        if (op.Token.Value == ".")
            return null;

        entries.RemoveAt(entries.Count - 1);
        entries.RemoveAt(entries.Count - 1);
        entries.RemoveAt(entries.Count - 1);

        InternalExpression expr1 = op1.Expression;
        InternalExpression expr2 = op2.Expression;
        WToken oper = op.Token;

        BinaryOpExpression bin_op_expr = new BinaryOpExpression(oper.Value);
        bin_op_expr.SetA(expr1);
        bin_op_expr.SetB(expr2);

        return new ParseStackEntry(bin_op_expr);
    }

    // ID -> ID_EXPR
    static ParseStackEntry REDUCE_TOPLEVEL_ID_EXPR(List<ParseStackEntry> entries, WScope scope)
    {
        if (entries.Count < 1)
            return null;

        ParseStackEntry expr_entry = entries[entries.Count - 1];

        if (!expr_entry.IsToken || expr_entry.Token.Type != WToken.ID)
            return null;


        entries.RemoveAt(entries.Count - 1);

        IDExpression expr = new IDExpression(expr_entry.Token.Value, scope);

        return new ParseStackEntry(expr);
    }

    // ID_EXPR.ID -> ID_EXPR
    static ParseStackEntry REDUCE_ID_EXPR(List<ParseStackEntry> entries, WScope scope)
    {
        if (entries.Count < 3)
            return null;

        ParseStackEntry expr_entry = entries[entries.Count - 1];
        ParseStackEntry dot = entries[entries.Count - 2];
        ParseStackEntry parent_expr_entry = entries[entries.Count - 3];

        if (!dot.IsToken || dot.Token.Value != ".")
            return null;

        if (!expr_entry.IsToken || expr_entry.Token.Type != WToken.ID)
            return null;

        if (!parent_expr_entry.IsExpression)
            return null;

        entries.RemoveAt(entries.Count - 1);
        entries.RemoveAt(entries.Count - 1);
        entries.RemoveAt(entries.Count - 1);

        IDExpression expr = new IDExpression(expr_entry.Token.Value, parent_expr_entry.Expression);

        return new ParseStackEntry(expr);
    }

    static ParseStackEntry REDUCE_EMPTY_FUNC_EXPR(List<ParseStackEntry> entries, WScope scope)
    {
        if (entries.Count < 3)
            return null;

        ParseStackEntry id_entry = entries[entries.Count - 3];
        ParseStackEntry parens_1 = entries[entries.Count - 2];
        ParseStackEntry parens_2 = entries[entries.Count - 1];

        bool id_entry_is_valid = id_entry.IsExpression && (id_entry.Expression is IDExpression);

        if (!parens_1.IsToken || parens_1.Token.Value != "(")
            return null;

        if (!parens_2.IsToken || parens_2.Token.Value != ")")
            return null;

        if (!id_entry_is_valid)
        {
            return null;
        }

        entries.RemoveAt(entries.Count - 1);
        entries.RemoveAt(entries.Count - 1);
        entries.RemoveAt(entries.Count - 1);

        FuncExpression expr = new FuncExpression(
            (IDExpression)id_entry.Expression,
            new ExpressionList()
            );

        return new ParseStackEntry(expr);
    }

    // ID_EXPR '(' EXPR ')' -> FUNC_EXPR
    static ParseStackEntry REDUCE_FUNC_EXPR(List<ParseStackEntry> entries, WScope scope)
    {
        if (entries.Count < 4)
            return null;

        ParseStackEntry id_entry = entries[entries.Count - 4];
        ParseStackEntry parens_1 = entries[entries.Count - 3];
        ParseStackEntry expr_entry = entries[entries.Count - 2];
        ParseStackEntry parens_2 = entries[entries.Count - 1];

        bool id_entry_is_valid = id_entry.IsExpression && (id_entry.Expression is IDExpression);
        bool expr_entry_is_valid = expr_entry.IsExpression && ((expr_entry.Expression is ExpressionList) || expr_entry.Expression is LiteralExpression || expr_entry.Expression is IDExpression || expr_entry.Expression is BinaryOpExpression);

        if (!parens_1.IsToken || parens_1.Token.Value != "(")
            return null;

        if (!parens_2.IsToken || parens_2.Token.Value != ")")
            return null;

        if (!id_entry_is_valid || !expr_entry_is_valid)
        {
            return null;
        }

        entries.RemoveAt(entries.Count - 1);
        entries.RemoveAt(entries.Count - 1);
        entries.RemoveAt(entries.Count - 1);
        entries.RemoveAt(entries.Count - 1);

        if (expr_entry.Expression is ExpressionList)
        {

            FuncExpression expr = new FuncExpression(
                (IDExpression)id_entry.Expression,
                (ExpressionList)expr_entry.Expression
                );

            return new ParseStackEntry(expr);
        }
        else
        {
            ExpressionList list = new ExpressionList();
            list.AddExpression(expr_entry.Expression);

            FuncExpression expr = new FuncExpression(
                (IDExpression)id_entry.Expression,
                list
                );

            return new ParseStackEntry(expr);
        }
    }

    // EXPR ',' EXPR -> LIST_EXPR
    // LIST_EXPR ',' EXPR -> LIST_EXPR
    static ParseStackEntry REDUCE_LIST_EXPR(List<ParseStackEntry> entries, WScope scope)
    {
        if (entries.Count < 3)
            return null;

        ParseStackEntry list_item_1_expr = entries[entries.Count - 3];
        ParseStackEntry comma = entries[entries.Count - 2];
        ParseStackEntry list_item_2_expr = entries[entries.Count - 1];

        if (!comma.IsToken || comma.Token.Value != ",")
            return null;

        if(!list_item_1_expr.IsExpression || !list_item_2_expr.IsExpression)
        {
            return null;
        }

        // Remove the last 3 entries
        entries.RemoveAt(entries.Count - 1);
        entries.RemoveAt(entries.Count - 1);
        entries.RemoveAt(entries.Count - 1);

        if(list_item_1_expr.Expression is ExpressionList)
        {
            ExpressionList list = (ExpressionList)list_item_1_expr.Expression;
            list.AddExpression(list_item_2_expr.Expression);

            return new ParseStackEntry(list);
        }
        else
        {
            ExpressionList new_list = new ExpressionList();
            new_list.AddExpression(list_item_1_expr.Expression);
            new_list.AddExpression(list_item_2_expr.Expression);

            return new ParseStackEntry(new_list);
        }
    }

    static ParseStackEntry REDUCE_LITERAL_EXPR(List<ParseStackEntry> entries, WScope scope)
    {
        if (entries.Count < 1)
            return null;

        ParseStackEntry literal_Expr = entries[entries.Count - 1];

        if(!literal_Expr.IsToken)
        {
            return null;
        }

        
        if(literal_Expr.Token.Type != WToken.INT_LITERAL
            && literal_Expr.Token.Type != WToken.STR_LITERAL
            && literal_Expr.Token.Type != WToken.BOOL_LITERAL)
        {
            return null;
        }

        entries.RemoveAt(entries.Count - 1);

        WToken literal_token = literal_Expr.Token;

        object literal_value = null;

        if(literal_token.Type == WToken.INT_LITERAL)
        {
            literal_value = int.Parse(literal_token.Value);
        }else if(literal_token.Type == WToken.STR_LITERAL)
        {
            literal_value = literal_token.Value;
        }else if(literal_token.Type == WToken.BOOL_LITERAL)
        {
            literal_value = bool.Parse(literal_token.Value);
        }

        return new ParseStackEntry(new LiteralExpression(literal_value));
    }

    delegate ParseStackEntry ReduceAction(List<ParseStackEntry> entries, WScope scope);

    static bool try_reduce(ReduceAction action, List<ParseStackEntry> entries, WScope scope, out ParseStackEntry reduced_entry)
    {
        reduced_entry = action(entries, scope);
        if (reduced_entry != null)
            return true;
        return false;
    }

    // VASTLY IMPROVED expression generation using a shift reduce algorithm.
    // Definitely still has bugs, but can now handle more complicated expressions.
    void generateExpression_shiftReduce()
    {
        _consumedTokens = new List<WToken>();

        WToken token = shift();
        List<ParseStackEntry> parse_stack = new List<ParseStackEntry>();
        
        parse_stack.Add(new ParseStackEntry(token));
        do
        {
            WToken look_ahead = next();

            ParseStackEntry reduced_entry = null;

            if (try_reduce(REDUCE_LITERAL_EXPR, parse_stack, _scope, out reduced_entry)) { parse_stack.Add(reduced_entry); continue; }

            //if(look_ahead == null || look_ahead.Value != ".")
            if (try_reduce(REDUCE_ID_EXPR, parse_stack, _scope, out reduced_entry)) { parse_stack.Add(reduced_entry); continue; }

            if (try_reduce(REDUCE_TOPLEVEL_ID_EXPR, parse_stack, _scope, out reduced_entry)) { parse_stack.Add(reduced_entry); continue; }

            if(look_ahead == null || look_ahead.Type != WToken.OPERATOR)
                if (try_reduce(REDUCE_LIST_EXPR, parse_stack, _scope, out reduced_entry)) { parse_stack.Add(reduced_entry); continue; }

            if (look_ahead == null || (look_ahead.Value != "(" && look_ahead.Value != "."))
                if (try_reduce(REDUCE_BIN_OP_EXPR, parse_stack, _scope, out reduced_entry)) { parse_stack.Add(reduced_entry); continue; }

            if (try_reduce(REDUCE_FUNC_EXPR, parse_stack, _scope, out reduced_entry)) { parse_stack.Add(reduced_entry); continue; }
            if (try_reduce(REDUCE_EMPTY_FUNC_EXPR, parse_stack, _scope, out reduced_entry)) { parse_stack.Add(reduced_entry); continue; }

            token = shift();
            if(token != null)
                parse_stack.Add(new ParseStackEntry(token));
        } while (token != null);

        if (parse_stack.Count > 1)
        {
            dumpParseStack(parse_stack);
            Debug.Log(dumpTokens(_consumedTokens));
            throw new System.Exception("Could not reduce stack to single expression. Size: " + _expressionStack.Count);
        }

        // Fail if there is no expression to evaluate
        if (parse_stack.Count == 0)
        {
            dumpParseStack(parse_stack);
            Debug.Log(dumpTokens(_consumedTokens));
            throw new System.Exception("Empty stack.");
        }

        if(!parse_stack[0].IsExpression)
        {
            dumpParseStack(parse_stack);
            Debug.Log(dumpTokens(_consumedTokens));
            throw new System.Exception("Parse stack was not reduced to an expression.");
        }

        _expression = parse_stack[0].Expression;
    }

    void dumpParseStack(List<ParseStackEntry> stack)
    {
        Dumper d = new Dumper("Parse Stack");
        d.AddProp("Size", stack.Count);
        d.AddDivider();
        for(int i = 0; i < stack.Count; ++i)
        {
            d.AddLine("[" + i + "] = " + stack[i].ToString());
        }

        Debug.Log(d.GetDump());
    }


    // Literally the worst thing!
    // Evaluates a token stream and chains together expressions.
    [System.Obsolete("Use generateExpression_shiftReduce instead.")]
	void generateExpression()
	{
		
		_consumedTokens = new List<WToken>();
		WToken token = shift();
		
        // Prepare your body
		do
		{
			if (token == null)
				throw new System.Exception(_scope + " Prematurely exhaused token stream near '" + dumpTokens(_consumedTokens) + "' ... in '" + dumpTokens(_tokens) + "' " + _tokens.Count);
			
			if (token.Type == WToken.INT_LITERAL)
			{
                // Literals 2 ez
				push(new LiteralExpression(int.Parse(token.Value)));
			}
			else if (token.Type == WToken.ID)
			{
                // Push the current token as an expression
				push(new IDExpression(token.Value, _scope));
				
                // Is the next token also an identifier?
				WToken n = next();
				if (n != null && n.Type == WToken.OPERATOR)
				{
                    // ex: Item.MyProperty
					if (n.Value == ".")
					{
                        // Get the next token, and let's go again.
						token = shift();
						continue;
					}
				}
				
                // Nope! It's not. What's at the front of the stack?
				InternalExpression front_expr = _expressionStack[0];

				if (front_expr is BinaryOpExpression)
				{
                    // This is a terrible way to do binary operations!
					BinaryOpExpression binOp = (BinaryOpExpression)front_expr;
					
					if (_expressionStack.Count > 1)
					{
                        // Set the operator in the binop!
						binOp.SetB(pop());
					}
				}
			}
			else if (token.Type == WToken.OPERATOR)
			{
                // Operator? WELL LETS HANDLE IT!
				BinaryOpExpression b = null;
				switch (token.Value)
				{
                // Other Operators
				case ".":
					push(new IDExpression(shift().Value, pop()));
					break;
                // Binary Operators
				case "+":
					b = new BinaryOpExpression(token.Value);
					b.SetA(pop());
					push(b);
					break;
				case "*":
					b = new BinaryOpExpression(token.Value);
					b.SetA(pop());
					push(b);
					break;
				case "/":
					b = new BinaryOpExpression(token.Value);
					b.SetA(pop());
					push(b);
					break;
				case "-":
					b = new BinaryOpExpression(token.Value);
					b.SetA(pop());
					push(b);
					break;
				case "%":
					b = new BinaryOpExpression(token.Value);
					b.SetA(pop());
					push(b);
					break;
				case ">":
					b = new BinaryOpExpression(token.Value);
					b.SetA(pop());
					push(b);
					break;
				case "<":
					b = new BinaryOpExpression(token.Value);
					b.SetA(pop());
					push(b);
					break;
				case "<=":
					b = new BinaryOpExpression(token.Value);
					b.SetA(pop());
					push(b);
					break;
				case ">=":
					b = new BinaryOpExpression(token.Value);
					b.SetA(pop());
					push(b);
					break;
				case "&&":
					b = new BinaryOpExpression(token.Value);
					b.SetA(pop());
					push(b);
					break;
                case "==":
                    b = new BinaryOpExpression(token.Value);
                    b.SetA(pop());
                    push(b);
                    break;
				default:
					throw new System.Exception("Unhandled operator '" + token.Value + "' at " + dumpTokens(_consumedTokens));
				}
			}
			else if (token.Type == WToken.LIST_OPERATOR)
			{
				switch (token.Value)
				{
				case ",":
                    // Go on...
					token = shift();
					continue;
				case "(":
                    // Assumes the presence of an open parens indicates the beginning of a
                    // paramter list.
					ExpressionList list = new ExpressionList();
					push(list);
					token = shift();
					continue;
				case ")":
                    // Assumes the presence of a close parens indicates the ending of a 
                    // parameter list. Handles that madness.

					ExpressionList foundList = null;
					int foundListAt = 0;
					for (int i = _expressionStack.Count - 1; i > 0; i--)
					{
						InternalExpression e = _expressionStack.ElementAt(i);
						if (e is ExpressionList)
						{
							foundList = (ExpressionList)e;
							_expressionStack.RemoveAt(i);
							foundListAt = i - 1;
							break;
						}
					}
					
					if (foundList == null)
						throw new System.Exception("Could not complete parameter list. Possibly missing initial '('.");
					
					for (int i = _expressionStack.Count - 1; i > foundListAt; i--)
					{
						InternalExpression e = _expressionStack.ElementAt(i);
						foundList.AddExpression(e);
						_expressionStack.RemoveAt(i);
					}
					
					InternalExpression backExpr = _expressionStack[foundListAt];
					if (backExpr is IDExpression)
					{
						IDExpression idexpr = (IDExpression)pop();
						FuncExpression func = new FuncExpression(idexpr, foundList);
						push(func);
					}
					
					InternalExpression front_expr = _expressionStack[0];
					if (front_expr is BinaryOpExpression)
					{
						if (_expressionStack.Count > 1)
						{
							BinaryOpExpression binOp = (BinaryOpExpression)front_expr;
							binOp.SetB(pop());
						}
					}
					
					break;
				case "[":
                    // Assumes a left brace indicates the beginning of an indice into an array
					token = shift ();
					if(token.Type == WToken.INT_LITERAL)
					{
						push(new LiteralExpression(int.Parse(token.Value)));
					}else if(token.Type == WToken.ID)
					{
						push (new IDExpression(token.Value, _scope));
					}
					break;
				case "]":
                    // Assumes the right brace indicates the ending of an indice into an array
					InternalExpression indexExpr = pop();
					IDExpression idExpr = (IDExpression)pop ();
					ArrayIndexExpression arrayIndexExpr = new ArrayIndexExpression(idExpr, indexExpr);
					push (arrayIndexExpr);
					break;
				default:
					throw new System.Exception("Unhandled list operator '" + token.Value + "' at " + dumpTokens(_consumedTokens));
				}
			}
			
			token = shift();
			
            // No token? All gone?
			if (token == null)
			{
                // Wrap up any hanging binop expressions... I guess
				InternalExpression front_expr = _expressionStack[0];
				if (front_expr is BinaryOpExpression)
				{
					if (_expressionStack.Count > 1)
					{
						BinaryOpExpression binOp = (BinaryOpExpression)front_expr;
						binOp.SetB(pop());
					}
				}
			}
		}
		while (token != null);
		
        // Fail if we have not simplified the stack down to a single expression
		if (_expressionStack.Count > 1)
		{
			Dump();
			throw new System.Exception("Could not reduce stack. Size: " + _expressionStack.Count);
		}
		
        // Fail if there is no expression to evaluate
		if (_expressionStack.Count == 0)
		{
			Dump();
			throw new System.Exception("Nothing left on the stack.");
		}
		
        // W E D I D I T B O Y Z
		_expression = _expressionStack[0];
	}

	string dumpTokens(List<WToken> tokens)
	{
		string dump = "";
		foreach (WToken t in tokens)
			dump += "|" + t.Value;
		return dump;
	}

    // Cached evaluation.. 
	object _lastEvaluation = null;

}
